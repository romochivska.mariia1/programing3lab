﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

[Serializable]
public class Student
{
    [XmlAttribute("LastName")]
    public string LastName { get; set; }

    [XmlAttribute("Faculty")]
    public string Faculty { get; set; }

    [XmlAttribute("Course")]
    public int Course { get; set; }

    [XmlAttribute("Gender")]
    public string Gender { get; set; }

    [XmlAttribute("Scholarship")]
    public double Scholarship { get; set; }

    public List<double> Grades { get; set; }
}

internal class Program
{
    static void Main(string[] args)
    {
        string fileName = "Student.xml";

        CreateXmlFile(fileName);
        DisplayXmlFile(fileName);

        string searchLastName = "Коваленко";
        DisplayStudentInfoByLastName(fileName, searchLastName);

        string faculty = "Факультет ФМЦТ";
        int course = 3;
        CountExcellentMaleStudents(fileName, faculty, course);
    }

    static void CreateXmlFile(string fileName)
    {
        List<Student> students = new List<Student>
        {
            new Student
            {
                LastName = "Петренко",
                Faculty = "Факультет Філологічний",
                Course = 1,
                Gender = "чоловік",
                Scholarship = 2300.0,
                Grades = new List<double> { 90.5, 85.5, 88.0 }
            },
            new Student
            {
                LastName = "Іваненко",
                Faculty = "Факультет Економічний",
                Course = 2,
                Gender = "чоловік",
                Scholarship = 2500.0,
                Grades = new List<double> { 78.0, 88.5, 92.0 }
            },
            new Student
            {
                LastName = "Коваленко",
                Faculty = "Факультет ФМЦТ",
                Course = 3,
                Gender = "чоловік",
                Scholarship = 2800.0,
                Grades = new List<double> { 95.0, 96.5, 91.0 }
            }
        };

        XmlSerializer serializer = new XmlSerializer(typeof(List<Student>));

        using (TextWriter writer = new StreamWriter(fileName))
        {
            serializer.Serialize(writer, students);
        }

        Console.WriteLine("XML-файл створено: " + fileName);
    }

    static void DisplayXmlFile(string fileName)
    {
        List<Student> students = DeserializeStudents(fileName);
        Console.WriteLine("Вміст XML-файлу:");

        foreach (var student in students)
        {
            Console.WriteLine("Прізвище: " + student.LastName);
            Console.WriteLine("Факультет: " + student.Faculty);
            Console.WriteLine("Курс: " + student.Course);
            Console.WriteLine("Стать: " + student.Gender);
            Console.WriteLine("Студентська стипендія: " + student.Scholarship);

            foreach (var grade in student.Grades)
            {
                Console.WriteLine("Оцінка: " + grade);
            }
        }
    }

    static void DisplayStudentInfoByLastName(string fileName, string lastName)
    {
        List<Student> students = DeserializeStudents(fileName);

        var matchingStudents = students.Where(s => s.LastName == lastName).ToList();

        if (matchingStudents.Count > 0)
        {
            foreach (var student in matchingStudents)
            {
                Console.WriteLine("Прізвище: " + student.LastName);
                Console.WriteLine("Факультет: " + student.Faculty);
                Console.WriteLine("Курс: " + student.Course);
                Console.WriteLine("Стать: " + student.Gender);
                Console.WriteLine("Студентська стипендія: " + student.Scholarship);

                foreach (var grade in student.Grades)
                {
                    Console.WriteLine("Оцінка: " + grade);
                }
            }
        }
        else
        {
            Console.WriteLine("Студента з прізвищем " + lastName + " не знайдено.");
        }
    }

    static void CountExcellentMaleStudents(string fileName, string faculty, int course)
    {
        List<Student> students = DeserializeStudents(fileName);

        var excellentCount = students.Count(s => s.Faculty == faculty && s.Course == course && s.Gender == "чоловік" && s.Grades.All(grade => grade >= 90.0));

        Console.WriteLine($"Кількість хлопців-відмінників на {course}-му курсі факультету {faculty}: {excellentCount}");
    }

    static List<Student> DeserializeStudents(string fileName)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(List<Student>));

        using (TextReader reader = new StreamReader(fileName))
        {
            return (List<Student>)serializer.Deserialize(reader);
        }
    }
}
